package agenda.objetos;

public class Nota {
	private String fichero;
	private String nombreFichero;
	
	public Nota() {
		fichero = "";
	}
	
	public String getFichero() {
		return fichero;
	}
	public void setFichero(String nota) {
		this.fichero = nota;
	}
	public String getNombreFichero() {
		return nombreFichero;
	}
	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}
}
