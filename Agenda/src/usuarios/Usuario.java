package usuarios;
import agenda.objetos.Nota;

public abstract class Usuario {

	protected String nick;
	protected String password;
	protected int edad;
	protected Nota notasUsuario;
	
	public Usuario() {
		notasUsuario = new Nota();
		edad = 0;
		nick = null;
		password =null;
	}
	
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public abstract String toString();
	
	public abstract void modificarUsuario(Usuario unUsuario);
	
	public Nota getNotasUsuario() {
		return notasUsuario;
	}
	public void setNotasUsuario(Nota notasUsuario) {
		this.notasUsuario = notasUsuario;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
		
	
}
